import firebase from 'firebase';
import { WebService } from './WebService';
import { DatabaseService } from './database.service';
import { Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { SignInPage } from '../pages/sign-in/sign-in';
@Injectable()
export class AuthService{
    email = "";
    constructor(private webSvc:WebService, private dbSvc:DatabaseService){}
    
    signup(email: string, password: string){
        return firebase.auth().createUserWithEmailAndPassword(email,password)
    }
    signin(email:string , password:string){
        return firebase.auth().signInWithEmailAndPassword(email,password);
    }
    logout(){
        firebase.auth().signOut();
    }

    getId(onSuccess:Function){
        let query = "SELECT id FROM tbl_user";
        this.dbSvc.query(query, (data:any)=>{
          let len = data.rows.length;
          let id = data.rows.item(0).id
          onSuccess(id);
        }, ()=>{})
    }
}