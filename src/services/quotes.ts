import { Quote } from '../data/quote.interface'; 

export class QuotesService { 
    private favoriteQuotes: Quote[] = []; 
    addQuoteToFavorites(quote: Quote) { 
        this.favoriteQuotes.push(quote); 
        
    } 
    removeQuoteFromFavorites(quote: Quote) {
        let x = this.favoriteQuotes.indexOf(quote);
        this.favoriteQuotes.splice(x,1);
    } 
    getFavoriteQuotes() {
        return this.favoriteQuotes.slice();

    } 
    isFavorite(quote: Quote) {
        if(this.favoriteQuotes.indexOf(quote)>=0){
            return true;
        }
        else return false;
    } }