export class LocationService{
    private loc:{title:String, description:String}[]=[];

    add(title:String, description:String){
        this.loc.push({title,description})
    }
    get(){
        return this.loc;
    }
}