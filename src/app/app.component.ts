import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController,NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import firebase from 'firebase';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AddEventPage } from '../pages/add-event/add-event';
import { DatabaseService } from '../services/database.service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  isLogin = false;
  rootPage: any = SignInPage;
  tabsPage = TabsPage;
  settingsPage = SettingsPage;
  signInPage = SignInPage;
  signUpPage = SignUpPage;
  addEventPage = AddEventPage;
  $signed;

  @ViewChild('sideMenuContent') nav: NavController;
  

  constructor(platform: Platform,  statusBar: StatusBar, 
    
    splashScreen: SplashScreen, private menuCtrl: MenuController,  private dbSvc:DatabaseService) {
      firebase.initializeApp({
        apiKey: "AIzaSyB9qQCQOKAdTj2yEOaW0u9HbGXO7EhdnTE",
        authDomain: "project-akhir-2017.firebaseapp.com"
    })
    firebase.auth().onAuthStateChanged(user =>{
      if(user){
        this.isLogin = true;
        this.nav.setRoot(TabsPage);
      }
      else{
        this.isLogin = false;
        this.nav.setRoot(SignInPage);
      }
    })
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.asdads
      statusBar.styleDefault();
      splashScreen.hide();
    })
  } 
}
