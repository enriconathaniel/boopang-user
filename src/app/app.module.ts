import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddQuotePage } from '../pages/add-quote/add-quote';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { QuotesPage } from '../pages/quotes/quotes';
import { QuotesService } from '../services/quotes';
import { SettingsService } from '../services/settings';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AuthService } from '../services/authService';
import { CourtPage } from '../pages/court/court';
import { ProfilePage } from '../pages/profile/profile';
import { EventPage } from '../pages/event/event';
import { AddEventPage } from '../pages/add-event/add-event';
import { PromoPage } from '../pages/promo/promo';
import { HistoryPage } from '../pages/history/history';
import { AddCourtPage } from '../pages/add-court/add-court';
import { LocationService } from '../services/location';

import { AgmCoreModule } from "@agm/core";
import { NgCalendarModule  } from 'ionic2-calendar';
import { Geolocation } from '@ionic-native/geolocation';
import { SetLocationPage } from '../pages/set-location/set-location';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { TabBookingWaitingPage } from '../pages/tab-booking-waiting/tab-booking-waiting';
import { TabBookingOnProgressPage } from '../pages/tab-booking-on-progress/tab-booking-on-progress';
import { TabBookingDonePage } from '../pages/tab-booking-done/tab-booking-done';
import { TopUpPage } from '../pages/top-up/top-up';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { ImagePickerPage } from '../pages/image-picker/image-picker';
import { ChatPage } from '../pages/chat/chat';
import { DescriptionPage } from '../pages/description/description';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { BuktiPage } from '../pages/bukti/bukti';

import { Camera } from '@ionic-native/camera';
import { BookCourtPage } from '../pages/book-court/book-court';
import { SearchCourtPage } from '../pages/search-court/search-court';
import { BoopayHistoryPage } from '../pages/boopay-history/boopay-history';
import { WebService } from '../services/WebService';
import { DatabaseService } from '../services/database.service';
import { SQLite } from '@ionic-native/sqlite';
import { HttpModule} from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    SettingsPage,
    QuotesPage,
    AddQuotePage,
    SignInPage,
    SignUpPage,
    ProfilePage,
    CourtPage,
    HistoryPage,
    EventPage,
    AddEventPage,
    PromoPage,
    AddCourtPage,
    SetLocationPage,
    ChangePasswordPage,
    TabBookingWaitingPage,
    TabBookingOnProgressPage,
    TabBookingDonePage,
    TopUpPage,
    EditProfilePage,
    ImagePickerPage,
    ChatPage,
    DescriptionPage,
    PrivacyPolicyPage,
    BuktiPage,
    TermsOfServicePage,
    BookCourtPage,
    SearchCourtPage,
    BoopayHistoryPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'bottom'}),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyB9qQCQOKAdTj2yEOaW0u9HbGXO7EhdnTE"
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    SettingsPage,
    QuotesPage,
    AddQuotePage,
    SignInPage,
    SignUpPage,
    ProfilePage,
    CourtPage,
    HistoryPage,
    EventPage,
    AddEventPage,
    PromoPage,
    AddCourtPage,
    SetLocationPage,
    ChangePasswordPage,
    TabBookingWaitingPage,
    TabBookingOnProgressPage,
    TabBookingDonePage,
    TopUpPage,
    EditProfilePage,
    ImagePickerPage,
    ChatPage,
    DescriptionPage,
    PrivacyPolicyPage,
    BuktiPage,
    TermsOfServicePage,
    BookCourtPage,
    SearchCourtPage,
    BoopayHistoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QuotesService,
    SettingsService,
    Geolocation,
    LocationService,
    AuthService,
    Camera,
    WebService,
    DatabaseService,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
