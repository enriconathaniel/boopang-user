import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
import { Quote } from '../../data/quote.interface';
/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage {
  quotesisi = this.navParams.get('quotes');
  constructor(private quotesService: QuotesService, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotesPage');
  }

  onShowAlert(item:Quote){
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      subTitle:'',
      message: 'Are you sure you want to add the quote to favorites?',
      buttons:[
        {
          text: 'Ok',
          handler:() => {
            this.quotesService.addQuoteToFavorites(item);
            console.log(this.quotesService);
          }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        handler:() => {
          console.log("NO is clicked.");
        }
    }

    ]
    });
    alert.present();
  }

  onShowUnfavoriteAlert(item:Quote){
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      subTitle:'',
      message: 'Are you sure you want to remove the quote from favorites?',
      buttons:[
        {
          text: 'Ok',
          handler:() => {
            this.quotesService.removeQuoteFromFavorites(item);
            console.log(this.quotesService);
          }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        handler:() => {
          console.log("NO is clicked.");
        }
    }

    ]
    });
    alert.present();
  }

  onAddQuote(quote: Quote) { 
    this.quotesService.addQuoteToFavorites(quote); 
  }

  isFavorite(quote: Quote){
    return this.quotesService.isFavorite(quote);
  }
}
