import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BookCourtPage } from '../book-court/book-court';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the SearchCourtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-court',
  templateUrl: 'search-court.html',
})
export class SearchCourtPage {
  courtSelected:any;
  
  cardColor: string = '#fff';

  regisGame=this.navParams.data;

  courtCollection:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
            private webService: WebService) {
  }

  ionViewWillEnter(){
    this.getAllCourt();
  }

  ionViewDidLoad() {
    this.getAllCourt();
    console.log('ionViewDidLoad SearchCourtPage');
  }
  showFilter(){

  }
  selectCourt(name, id, wk_price, wd_price){
    document.getElementById('footer_detail').style.display = "block";
    document.getElementById('court_detail').innerHTML = name;
    this.courtSelected = {
      'court_name' : name,
      'id_court' : id,
      'weekday_price': wd_price,
      'weekend_price':wk_price
    }
  }
  openBookCourtPage(){
    var gabungan = this.collect(this.courtSelected, this.regisGame)
    console.log(gabungan);
    this.navCtrl.push(BookCourtPage, gabungan);
  }

  getAllCourt(){
    let req = "hai";

    this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_court_user.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(JSON.stringify(responseData))
      if(responseData){
        this.courtCollection = responseData
      }
    }, error =>{
    })

  }

  collect(field1: any, field2: any) {
    var ret = {};
    var len = arguments.length;
    for (var i=0; i<len; i++) {
      for (var p in arguments[i]) {
        if (arguments[i].hasOwnProperty(p)) {
          ret[p] = arguments[i][p];
        }
      }
    }
    return ret;
  }

}
