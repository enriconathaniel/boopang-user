import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/authService';
import { SignUpPage } from '../sign-up/sign-up';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {
  tabsPage = TabsPage;
  SignInForm: FormGroup;
  constructor(
    public navCtrl: NavController, public navParams: NavParams, 
    private authService: AuthService, private app:App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  ngOnInit(){
    this.initializeForm()
  }

  openSignUpPage(){
    this.navCtrl.push(SignUpPage);
  }

  initializeForm(){
    this.SignInForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    })
  }

  onSubmit(){
    let x = this.authService.signin(this.SignInForm.value.email, this.SignInForm.value.password);   
    console.log(x);
   }

}
