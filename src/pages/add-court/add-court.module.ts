import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCourtPage } from './add-court';

@NgModule({
  declarations: [
    AddCourtPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCourtPage),
  ],
})
export class AddCourtPageModule {}
