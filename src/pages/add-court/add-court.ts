import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { SetLocationPage } from "../set-location/set-location";
import { Geolocation } from "@ionic-native/geolocation";

import { Loc } from "../../models/location";
import { LocationService } from "../../services/location";

/**
 * Generated class for the AddCourtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-court',
  templateUrl: 'add-court.html',
})
export class AddCourtPage {

  location: Loc;
  locationIsSet = false;
  lat = -6.178306;
  lng = 106.631889;
  constructor(public locServices:LocationService, public navCtrl: NavController, private modalCtrl: ModalController, private geoloc: Geolocation) {
    this.location = new Loc(this.lat, this.lng);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCourtPage');
    
  }

  onSubmit(f: NgForm) {
    console.log(f.value);
    this.locServices.add(f.value.title, f.value.description);
    console.log(this.locServices.get())
    this.navCtrl.pop();
  }
  onSetMarker(event: any) {
    this.location = new Loc(event.coords.lat, event.coords.lng);
  }

  onOpenMap() {
    const modal = this.modalCtrl.create(SetLocationPage);
    modal.present();
    modal.onDidDismiss((data) => {
      console.log(data);
      if(data){
        this.location = data;
        this.lat = this.location.lat;
        this.lng = this.location.lng;
      }
    });
  }
  onLocate() {
    this.geoloc.getCurrentPosition()
      .then(
        currLocation => {
          this.location = new Loc(currLocation.coords.latitude, currLocation.coords.longitude)
          this.lat = currLocation.coords.latitude;
          this.lng = currLocation.coords.longitude;
          this.locationIsSet = true;
          console.log(this.lat); 
          console.log(this.lng); 
        }
      )
      .catch(
        error => {
          console.log(error);
        }
      )
  }

}
