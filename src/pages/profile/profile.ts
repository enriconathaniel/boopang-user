import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { AuthService } from '../../services/authService';
import { ChangePasswordPage } from '../change-password/change-password';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { TopUpPage } from '../top-up/top-up';
import { HistoryPage } from '../history/history';
import { TermsOfServicePage } from '../terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';
import { SignInPage } from '../sign-in/sign-in';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  

  constructor(public modalCtrl : ModalController, private authSvc: AuthService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  historyPage(){
    this.navCtrl.push(HistoryPage);
  }

  onSuccess(){
    
  }
  changePassword(){
    this.navCtrl.push(ChangePasswordPage);
  }
  logout(){
    this.authSvc.logout();
  }
  openModalTopUp() {
    let modal = this.modalCtrl.create(TopUpPage);
    modal.present();
  }
  openEditProfilePage(){
    this.navCtrl.push(EditProfilePage);
  }
  openChangePasswordPage(){
    this.navCtrl.push(ChangePasswordPage);
  }
  openTermsOfService(){
    this.navCtrl.push(TermsOfServicePage);
  }
  openPrivacyPolicy(){
    this.navCtrl.push(PrivacyPolicyPage);
  }

}
