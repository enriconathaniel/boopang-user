import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { CourtPage } from '../court/court';
import { EventPage } from '../event/event';
import { AddEventPage } from '../add-event/add-event';
import { PromoPage } from '../promo/promo';
import { HistoryPage } from '../history/history';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  
    eventPage = EventPage;
    historyPage = HistoryPage;
    courtPage = CourtPage;
    profilePage = ProfilePage;
    addEventPage = AddEventPage;
    promoPage = PromoPage;
  
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad TabsPage');
    }
  
  }
