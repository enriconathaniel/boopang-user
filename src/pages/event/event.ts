import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Loading, LoadingController } from 'ionic-angular';
import { AddEventPage } from '../add-event/add-event';
import { ChatPage } from '../chat/chat';
import { WebService } from '../../services/WebService';
import firebase from 'firebase';
import { HistoryPage } from '../history/history';
import { SearchCourtPage } from '../search-court/search-court';

/**
 * Generated class for the EventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {
  events: Array<string>;
  status: string ='0';
  statusPrivate: boolean = true;
  joined: boolean = false;

  booroomCollection:any;

  loading: Loading;
  constructor(public nav: NavController, public alertCtrl: AlertController, 
    public navCtrl: NavController, public navParams: NavParams,
    public webService: WebService, public loadingCtrl: LoadingController) {
  }

  ngOnInit(){
    // this.setEvents();
  }

  // setEvents

  ionViewWillEnter(){
    this.getBooroom();
  }
  

  ionViewDidLoad() {
    this.getBooroom();
    console.log('ionViewDidLoad EventPage');
  }

  isPrivate(status){
    if(status == "PRIVATE"){
      return true;
    }
    else return false;
  }

  getBooroom(){
      let req = "hai"

      this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_booroom.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      if(responseData){
        this.booroomCollection = responseData
        console.log(JSON.stringify(responseData))
      }
    }, error =>{
    })
  }

  onLoad(page: any){
    this.navCtrl.setRoot(page);
  }

  addEvent(){
    this.navCtrl.push(AddEventPage);
  }

  openAlertJoin(status,id_booroom,password) {

    let email = firebase.auth().currentUser.email;
    //kalo public
    if(status == "PUBLIC"){
      let alert = this.alertCtrl.create({
        title: 'Confirm Join',
        message: 'Are you sure want to join this game? Once you join, you cannot leave',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log('Buy clicked');

              let req = {
                'id_booroom' : id_booroom,
                'email' : email,
                'status_payment' : 'PENDING',
              }

              this.webService.post("http://delthraze.esy.es/Boopang/API/regis_booroom_member.php", JSON.stringify(req), null).subscribe(response => {
                let responseData = JSON.parse(response["_body"]);
                console.log(JSON.stringify(responseData))
                if(responseData){
                  let alert = this.alertCtrl.create({
                    title: 'Join success',
                    subTitle: 'Enjoy your game',
                    buttons: [{
                      role: 'cancel',
                      handler: () => {
                        this.navCtrl.push(HistoryPage);
                      }
                    }]
                  });
                  alert.present();
              } else{
                let alert = this.alertCtrl.create({
                  title: 'Join success',
                  subTitle: 'Enjoy your game',
                  buttons: [{
                    role: 'cancel',
                    handler: () => {
                      this.navCtrl.push(HistoryPage);
                    }
                  }]
                });
                alert.present();
              }
              }, error =>{
              })
            }
          }
        ]
      });
      alert.present();
    }
    //kalo private
    else{
      let alert = this.alertCtrl.create({
        title: 'Confirm Join',
        message: 'Please enter the game password to join. Once you join, you cannot leave',
        inputs: [
          {
            name: 'password',
            placeholder: 'Password',
            type: 'password'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Join',
            handler: data => {
              console.log('Buy clicked');
              if(password == data.password){
                let req = {
                  'id_booroom' : id_booroom,
                  'email' : email,
                  'status_payment' : 'PENDING',
                }
  
                this.webService.post("http://delthraze.esy.es/Boopang/API/regis_booroom_member.php", JSON.stringify(req), null).subscribe(response => {
                  let responseData = JSON.parse(response["_body"]);
                  if(responseData){
                      let alert = this.alertCtrl.create({
                        title: 'Join success',
                        subTitle: 'Enjoy your game',
                        buttons: [{
                          role: 'cancel',
                          handler: () => {
                            this.navCtrl.push(HistoryPage);
                          }
                        }]
                      });
                      alert.present();
                  } else{
                    let alert = this.alertCtrl.create({
                      title: 'Join success',
                      subTitle: 'Enjoy your game',
                      buttons: [{
                        role: 'cancel',
                        handler: () => {
                          this.navCtrl.push(HistoryPage);
                        }
                      }]
                    });
                    alert.present();
                  }
                }, error =>{
                })
              }
            }
          }
        ]
      });
      alert.present();
    }
    
  }


  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
    });
    this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);
  }

  ngForLast(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }
  }

}
