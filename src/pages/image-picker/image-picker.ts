import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the ImagePickerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-image-picker',
  templateUrl: 'image-picker.html',
})
export class ImagePickerPage {
  galleryType = 'regular';

  constructor(public viewCtrl : ViewController, public navCtrl: NavController, 
      public navParams: NavParams, private webService: WebService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagePickerPage');
  }
  check(image){
    let req  = {
      'image' : "../assets/imgs/avatar"+image+".png"
    }
    this.webService.post("http://delthraze.esy.es/Boopang/API/insert_gambar_game.php", JSON.stringify(req), null).subscribe(response => {
        let responseData = JSON.parse(response["_body"]);
        console.log(JSON.stringify(responseData))
        if(responseData){
          this.navCtrl.pop();
        }
      }, error =>{
      })
    console.log("../assets/imgs/avatar"+image+".png");
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
