import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { BuktiPage } from '../bukti/bukti';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

/**
 * Generated class for the TopUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-top-up',
  templateUrl: 'top-up.html',
})
export class TopUpPage {

  constructor(public modalCtrl: ModalController ,public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopUpPage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  openModalBukti() {
    let modal = this.modalCtrl.create(BuktiPage);
    modal.present();
  }

}