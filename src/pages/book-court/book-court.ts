import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the BookCourtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-court',
  templateUrl: 'book-court.html',
})
export class BookCourtPage {
  events: Array<string>;
  status: string ='0';
  statusPrivate: boolean = true;
  joined: boolean = false;

  userInfo:any;

  booroomData:any;

  loading: Loading;

  allForm = this.navParams.data;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController, public webService: WebService, 
    public loadingCtrl: LoadingController) {
  }

  openAlertBook(){
    let confirm = this.alertCtrl.create({
      title: 'Book Now',
      message: 'Are you sure want to book this court? Once you click this button, you cannot cancel your booking.',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.checkingBoopay();
            this.navCtrl.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log(this.navParams.data)
    console.log('ionViewDidLoad BookCourtPage');
  }

  checkingBoopay(){

  }

  getBooroom(id_booroom){
    let req = id_booroom;

    this.webService.post("http://delthraze.esy.es/Boopang/API/get_booroom_data.php", JSON.stringify(req), null).subscribe(response => {
    let responseData = JSON.parse(response["_body"]);
    if(responseData){
      this.booroomData = responseData
      // this.getMemberBalance(id_booroom)
      console.log(JSON.stringify(responseData))
    }
  }, error =>{
  })

  // getMemberBalance(id_booroom){
  //   let req = id_booroom;

  //   this.webService.post("http://delthraze.esy.es/Boopang/API/get_booroom_data.php", JSON.stringify(req), null).subscribe(response => {
  //   let responseData = JSON.parse(response["_body"]);
  //   if(responseData){
  //     this.booroomData = responseData
  //     this.isMember();
  //     console.log(JSON.stringify(responseData))
  //   }
  // }, error =>{
  // })

  // isMember(){
  //   let len = this.booroomCollection.length;
  //   console.log(this.booroomCollection);

  //   for(let i = 0;i< len;i++){
  //     let req = {
  //       'id_user' : this.userInfo.id,
  //       'id_booroom' : this.booroomCollection[i].id
  //     }

  //     console.log(this.booroomCollection[i].id)

  //     this.webService.post("http://delthraze.esy.es/Boopang/API/cek_booroom_member.php", JSON.stringify(req), null).subscribe(response => {
  //     let responseData = JSON.parse(response["_body"]);
  //     if(responseData){
  //       document.getElementById(this.booroomCollection[i].id).style.display = "none";
  //     }
  //   }, error =>{
  //   })

  //   }
  }
}
