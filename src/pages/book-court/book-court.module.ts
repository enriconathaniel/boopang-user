import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookCourtPage } from './book-court';

@NgModule({
  declarations: [
    BookCourtPage,
  ],
  imports: [
    IonicPageModule.forChild(BookCourtPage),
  ],
})
export class BookCourtPageModule {}
