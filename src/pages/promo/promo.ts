import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { DescriptionPage } from '../description/description';
import { AddEventPage } from '../add-event/add-event';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the PromoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promo',
  templateUrl: 'promo.html',
})
export class PromoPage {
  showDescriptionClicked: boolean[] = [];

  eventCollection:any;
  loading:Loading;

  noPromo:boolean = false;
  errorHandling:boolean = false;

  constructor(public modalCtrl: ModalController ,public navCtrl: NavController, 
    public navParams: NavParams, public webService: WebService, public loadingCtrl: LoadingController) {
  }

  ionViewWillEnter(){
    //this.showLoading();
    this.getEvent();
  }
  
  ionViewDidLoad() {
    //this.showLoading();
    this.getEvent();
    console.log('ionViewDidLoad PromoPage');
  }
  OpenDescription(){
    let modal = this.modalCtrl.create(DescriptionPage);
    modal.present();
  }
  addNewEventPage(){
    this.navCtrl.push(AddEventPage);
  }

  showDescription(i){
    if(this.showDescriptionClicked[i] ==false){
      this.showDescriptionClicked[i] = true;
    }else{
      this.showDescriptionClicked[i] = false;
      
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  ngForLast(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }
  }

  getEvent(){
    let req = "hai"
    
    this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_event_user.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(responseData)
      if(responseData){
        this.eventCollection = responseData;
      }else{
        this.noPromo = true;
      }
    }, error =>{
        this.errorHandling = true;
    }) 
  }

}
