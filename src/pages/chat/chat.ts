import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AddCourtPage } from '../add-court/add-court';
import { BookCourtPage } from '../book-court/book-court';
import { SearchCourtPage } from '../search-court/search-court';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  ref;
	name;
	newmessage;
  messagesList;
  constructor(public alert : AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }
  bookCourtPage(){
    this.navCtrl.push(SearchCourtPage);
  }

  ionViewDidLoad() {
    console.log(this.navParams.data)
    console.log('ionViewDidLoad ChatPage');
    
  }

  send(){
    this.ref.push({
       name: this.name,
       message: this.newmessage
    });
   }

}
