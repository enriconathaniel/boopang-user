import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { AddCourtPage } from '../add-court/add-court';
import { ImagePickerPage } from '../image-picker/image-picker';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { WebService } from '../../services/WebService';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SearchCourtPage } from '../search-court/search-court';
import firebase from 'firebase';
import { HistoryPage } from '../history/history';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the AddEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {
  AddGameForm:FormGroup;
  addCourtPage = AddCourtPage;
  status;
  value;
  statusPrivate:boolean = false;

  gambar:any;
  loading:Loading;

  provinceCollection:any;
  cityCollection:any;

  email = firebase.auth().currentUser.email;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public loadingCtrl: LoadingController,
    public webService: WebService, public builder: FormBuilder,
    private alertCtrl:AlertController) {
      
  }

  ionViewWillEnter(){
    this.getProvince();
  }

  ionViewDidLoad() {
    this.getProvince();
    console.log('ionViewDidLoad AddEventPage');
  }

  ngOnInit(){
    this.formCheck();
  }

  onChangeProvince(province){
    this.getCity(province);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  formCheck(){
    this.AddGameForm =this.builder.group({
      game_name: new FormControl(null,Validators.required),
      game_type: new FormControl(null,Validators.required),
      game_start: new FormControl(null,Validators.required),
      start_hour: new FormControl(null,Validators.required),
      end_hour: new FormControl(null,Validators.required),
      court_province: new FormControl(null,Validators.required),
      court_city:new FormControl(null,Validators.required),
      privacy:new FormControl(null,Validators.required),
      password_privacy:new FormControl(null),
      jumlah_pemain:new FormControl(null,Validators.required),
      email : this.email
    })
  }

  onSubmit(){
    console.log(this.AddGameForm.value)
    let req = this.AddGameForm.value;

    this.webService.post("http://delthraze.esy.es/Boopang/API/regis_booroom.php", JSON.stringify(req), null).subscribe(response => {
        let responseData = JSON.parse(response["_body"]);
        console.log(JSON.stringify(responseData))
        if(responseData){
          let alert = this.alertCtrl.create({
            title: 'Join success',
            subTitle: 'Enjoy your game',
            buttons: [{
              text:'ok',
              role: 'cancel',
              handler: () => {
                this.navCtrl.push(HistoryPage);
              }
            }]
          });
          alert.present();
        } else{
          let alert = this.alertCtrl.create({
            title: 'Regis Failed',
            subTitle: 'Try Again',
            buttons: [{
              text:'ok',
              role: 'cancel'
            }]
          });
          alert.present();
        }
      }, error =>{
      })

  }

  onChange(selectedValue: any){
    console.log(selectedValue);
    if (selectedValue == '1'){
      this.value = 1; 
      console.log(selectedValue);
    }
    else if (selectedValue == '2'){
      this.value = 0; 
      console.log(selectedValue);
    }
}
onSelectChange(selectedValue: any) {
  console.log('Selected', selectedValue);
}

getProvince(){
  let req = "hai"
  
  this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_province.php", JSON.stringify(req), null).subscribe(response => {
    let responseData = JSON.parse(response["_body"]);
    console.log(responseData)
    if(responseData){
      this.provinceCollection = responseData;
      console.log(this.provinceCollection)
    }
  }, error =>{
  })        
}

getCity(id_province){
  let req = {
    "id_province" : id_province,
  }
  
  this.webService.post("http://delthraze.esy.es/Boopang/API/get_all_city.php", JSON.stringify(req), null).subscribe(response => {
    let responseData = JSON.parse(response["_body"]);
    console.log(responseData)
    if(responseData){
      this.cityCollection = responseData;
    }
  }, error =>{
  })
}

}
