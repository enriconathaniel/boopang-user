import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CourtPage } from './court';

@NgModule({
  declarations: [
    CourtPage,
  ],
  imports: [
    IonicPageModule.forChild(CourtPage),
  ],
})
export class CourtPageModule {}
