import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuktiPage } from './bukti';

@NgModule({
  declarations: [
    BuktiPage,
  ],
  imports: [
    IonicPageModule.forChild(BuktiPage),
  ],
})
export class BuktiPageModule {}
