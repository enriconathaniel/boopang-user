import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SignInPage } from '../sign-in/sign-in';
import { AuthService } from '../../services/authService';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  tabsPage = TabsPage;
  SignUpForm: FormGroup;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private AuthService: AuthService, private builder: FormBuilder,
    private webService: WebService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  ngOnInit(){
    this.formCheck()
  }

  openSignInPage(){
    this.navCtrl.push(SignInPage);
  }


  formCheck(){
    this.SignUpForm = this.builder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required])],
      no_hp:['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])],
      confirm_password: ['', Validators.required],
    }, {validator: this.matchingPasswords('password', 'confirm_password')});
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    // TODO maybe use this https://github.com/yuyang041060120/ng2-validation#notequalto-1
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  onSubmit(){
    let req = {
      'first_name' : this.SignUpForm.value.first_name,
      'last_name' : this.SignUpForm.value.last_name,
      'email' : this.SignUpForm.value.email,
      'phone_number' : this.SignUpForm.value.no_hp,
      'password' : this.SignUpForm.value.password,
      'type' : 'USER'
    }

    console.log(req)

    this.webService.post("http://delthraze.esy.es/Boopang/API/sign_up.php", JSON.stringify(req), null).subscribe(response => {
      let responseData = JSON.parse(response["_body"]);
      console.log(responseData)
      if(responseData){
        let x = this.AuthService.signup(this.SignUpForm.value.email, this.SignUpForm.value.password);
        console.log(x);
      }
    }, error =>{
    })

    // let x = this.AuthService.signup(
    //   this.SignUpForm.value.first_name, this.SignUpForm.value.last_name,
    //   this.SignUpForm.value.email, this.SignUpForm.value.no_hp, 
    //   this.SignUpForm.value.password, ()=>{ console.log("Kepangil")});
  }

}
