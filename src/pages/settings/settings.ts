import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Toggle, ToastController, Config } from 'ionic-angular';
import { SettingsService } from '../../services/settings';
import { AuthService } from '../../services/authService';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  tabs : string;
  transition : string;
  constructor(private authSvc: AuthService, private config: Config , public toastCtrl: ToastController , private settingSvc: SettingsService , public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  signOut(){
    //this.authSvc.logout();
  }
  

  onToggle(toggle: Toggle){
      this.settingSvc.setBackground(toggle.checked);

  }
  isChecked(){
      return this.settingSvc.isAltBackground();
  }

  presentToast(){
    const toast = this.toastCtrl.create({
      message: 'Config has been applied',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  applyConfig(){
    this.config.set('ios','tabsPlacement',this.tabs);
    this.config.set('android','tabsPlacement',this.tabs);
    this.config.set('ios','pageTransition',this.transition);
    this.config.set('android','pageTransition',this.transition);
    this.presentToast();
  }

}
