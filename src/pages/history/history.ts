import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import { TabBookingWaitingPage } from '../tab-booking-waiting/tab-booking-waiting';
import { TabBookingOnProgressPage } from '../tab-booking-on-progress/tab-booking-on-progress';
import { TabBookingDonePage } from '../tab-booking-done/tab-booking-done';

import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as moment from 'moment';
import { ChatPage } from '../chat/chat';
import { firestore } from 'firebase';

import firebase from 'firebase';
import { WebService } from '../../services/WebService';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  tabWaiting=TabBookingWaitingPage;
  tabOnProgress=TabBookingOnProgressPage;
  tabDone=TabBookingDonePage;
  eventSource = [];
  viewTitle: string;
  selectedDay = new Date();
 
  calendar = {
    mode: 'month',
    currentDate: new Date()
  };
    booking: string;
    booroomCollection:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl:ModalController, private alertCtrl:AlertController,
      private webService:WebService) {
    this.booking = "bookingWaiting"
  }

  ionViewWillEnter(){
    this.getBooroom();
  }

  ionViewDidLoad() {
    this.getBooroom()
    console.log('ionViewDidLoad HistoryPage');
  }

  addEvent() {
    let modal = this.modalCtrl.create('AddSchedulePage', {selectedDay: this.selectedDay});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        let eventData = data;
 
        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);
 
        let events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  getBooroom(){
    let email = firebase.auth().currentUser.email;
    let req = {
      'email' : email
    }
    this.webService.post("http://delthraze.esy.es/Boopang/API/get_booroom_each_user.php", JSON.stringify(req), null).subscribe(response => {
        let responseData = JSON.parse(response["_body"]);
        console.log(JSON.stringify(responseData))
        if(responseData){
          this.booroomCollection = responseData
        }
      }, error =>{
      })

  }
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
 
  onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');
    
    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK']
    })
    alert.present();
  }
 
  onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }

  
  openAlertDeny(){
    let alert = this.alertCtrl.create({
      title: 'Booking Denied',
      message: "Please specify the reason",
      inputs: [
        {
          name: 'reason',
          placeholder: 'Reason'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    alert.present();
  }
  openAlertAccept(){
    let alert = this.alertCtrl.create({
      title: 'Booking Accept',
      message: "Additional notes",
      inputs: [
        {
          name: 'notes',
          placeholder: 'notes'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    alert.present();
  }
  openChatPage(){
    this.navCtrl.push(ChatPage);
  }
}
